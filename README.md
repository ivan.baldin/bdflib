bdflib
======

bdflib is a Python library for working with [BDF font files][bdf].

The library allows for manipulating fonts directly and comes with
command-line utilities for performing various operations on font files.

[bdf]: http://en.wikipedia.org/wiki/BDF_%28Glyph_Bitmap_Distribution_Format%29


Documentation
-------------

Documentation is available for [the current stable release][stable]
and for [the latest development version][latest].

[stable]: https://bdflib.readthedocs.io/en/stable/
[latest]: https://bdflib.readthedocs.io/en/latest/
