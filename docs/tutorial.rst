Tutorial
========

This document will guide you through installing the ``bdflib`` library,
and using it to read, modify and write a font.

Installation
------------

For this tutorial,
we will install ``bdflib`` in a "virtual environment"
so it doesn't interfere with any other Python libraries or applications
that might be installed on your system.

1. Create a virtual environment named ``bdflib-tutorial``:

   .. code-block:: console

      $ python3 -m venv bdflib-tutorial

2. Install the ``bdflib`` library into the virtual environment:

   .. code-block:: console

      $ bdflib-tutorial/bin/pip install bdflib

3. Launch Python from the virtual environment:

   .. code-block:: console

      $ bdflib-tutorial/bin/python

4. Verify that ``bdflib`` is properly installed:

   .. doctest::

      >>> import bdflib
      >>> bdflib.__version__
      '1.1.2'

Reading an existing font
------------------------

The most basic thing you'll want to do with ``bdflib``
is to read in an existing font in the BDF format.
For this tutorial,
we'll use the following font.
Copy and paste it
into a text editor and save it as ``tutorial.bdf``,
or :download:`download it <tutorial.bdf>`:

.. literalinclude:: tutorial.bdf

Reading in a BDF file is really easy:

    >>> from bdflib import reader
    >>> with open("docs/tutorial.bdf", "rb") as handle:
    ...     font = reader.read_bdf(handle)

Of course,
instead of ``docs/tutorial.bdf``
you should use the actual path to your ``tutorial.bdf`` file.

When you call :func:`.read_bdf`,
you get back a :class:`.Font` object
which represents the font, its metadata, and all its glyphs.
For example, our font has a name and a point-size:

.. doctest::
    :pyversion: >= 3.0

    >>> font[b"FACE_NAME"]
    b'Tutorial Font'
    >>> font[b"POINT_SIZE"]
    9.0

Our font also has a glyph for lowercase letter "o":

    >>> letter_o = font[ord("o")]

``letter_o`` is a :class:`.Glyph` object
representing the glyph and its properties.

.. doctest::
    :pyversion: >= 3.0

    >>> letter_o.name
    b'LATIN SMALL LETTER O'

To help debugging,
``bdflib`` can render a glyph bitmap to a printable string:

    >>> print(letter_o)
    |###.
    #...#
    #...#
    #...#
    +###-

* ``#`` characters represent pixels that are drawn
* ``.`` characters represent pixels that are blank
* ``-`` and ``|`` represent the X and Y axes
  (mostly obscured by drawn pixels in the above example)
* ``+`` represents the origin where they cross over

When you draw a glyph at a particular point on-screen,
the glyph's origin is put on that exact point.
It's usually the bottom-left corner of the glyph,
but some glyphs may start above and to the right
(like an apostrophe)
and some glyphs may start below and to the left
(like a lower-case ``j``).

Adding a glyph
--------------

Once you have a :class:`.Font` object,
you can modify it.
For example,
you can add a new glyph
with the :meth:`.new_glyph_from_data` method.
Let's add a glyph for U+0302 COMBINING CIRCUMFLEX ACCENT.

We start by designing our glyph.
A circumflex is an angle pointing up,
and at the size of our letter "o", it might look like this::

    ..#..
    .#.#.
    #...#

BDF files encode each row of the glyph
as a binary number,
where a 1 means a drawn pixel and a 0 means no pixel.
In Python, that might look like this::

    0b00100
    0b01010
    0b10001

More specifically,
BDF files store these binary numbers encoded in hexadecimal.
Each hexadecimal digit represents four bits,
but our glyph is five bits wide,
so we need to pad our numbers to eight bits wide
(the next higher multiple of four)::

    0b00100000
    0b01010000
    0b10001000

If we convert that to hexadecimal ...

    >>> raw_glyph_data = [
    ...     0b00100000,
    ...     0b01010000,
    ...     0b10001000,
    ... ]
    >>> for row in raw_glyph_data:
    ...     print("{:x}".format(row))
    20
    50
    88

... we get the numbers we'll need.

Now we can add our new glyph:

    >>> combining_circumflex = font.new_glyph_from_data(
    ...     # We might as well use the official Unicode character name.
    ...     name=b"COMBINING CIRCUMFLEX ACCENT",
    ...     # The hexadecimal encoding we calculated above.
    ...     data=[b"20", b"50", b"88"],
    ...     # We don't need to shift this glyph to the left or right.
    ...     bbX=0,
    ...     # Capitals for this font are 9px high, then leave a 1px gap.
    ...     bbY=10,
    ...     # This glyph is 5px wide.
    ...     bbW=5,
    ...     # This glyph is 3px tall.
    ...     bbH=3,
    ...     # 5px wide, 1px gap, so the next character is 6px to the right.
    ...     advance=6,
    ...     # This is character U+0302.
    ...     codepoint=0x0302,
    ... )

And now we have our circumflex glyph,
ready to combine with anything else
without overlapping:

    >>> print(combining_circumflex)
    |.#..
    |#.#.
    #...#
    |....
    |....
    |....
    |....
    |....
    |....
    |....
    |....
    |....
    +----

Merging glyphs
--------------

We have a glyph for LATIN SMALL LETTER O,
and we have a glyph for COMBINING CIRCUMFLEX ACCENT,
but there's also a Unicode character named
LATIN SMALL LETTER O WITH CIRCUMFLEX.
We could just draw it ourselves from scratch,
like we did with COMBINING CIRCUMFLEX ACCENT,
but it seems a waste of energy
when we already have the component glyphs available.
Luckily,
the :meth:`.Glyph.merge_glyph()` method
makes it easy to, well, merge two existing glyphs together.

First, we'll need to create the new glyph
using the new name and codepoint,
but re-using other properties from the base glyph:

    >>> letter_o_with_circumflex = font.new_glyph_from_data(
    ...     name=b"LATIN SMALL LETTER O WITH CIRCUMFLEX",
    ...     data=letter_o.get_data(),
    ...     bbX=letter_o.bbX,
    ...     bbY=letter_o.bbY,
    ...     bbW=letter_o.bbW,
    ...     bbH=letter_o.bbH,
    ...     advance=letter_o.advance,
    ...     codepoint=0x00F4,
    ... )

Next, we'll merge the circumflex glyph on top.
Since the circumflex glyph was positioned
to sit above nine-pixel-tall capitals
but this is a five-pixel-tall lowercase letter,
we'll need to move the circumflex down by four pixels:

    >>> letter_o_with_circumflex.merge_glyph(
    ...     other=combining_circumflex,
    ...     atX=0,
    ...     atY=-4,
    ... )

The result is a lowercase "o"
with a circumflex resting snugly on top!

    >>> print(letter_o_with_circumflex)
    |.#..
    |#.#.
    #...#
    |....
    |###.
    #...#
    #...#
    #...#
    +###-

If you want to automatically generate
as many glyphs as possible
from the base and combining glyphs in your font,
take a look at the :mod:`.glyph_combining` module.

Writing a font
--------------

Now that we've customised our font,
we can write the result to a new file:

    >>> from bdflib import writer
    >>> with open("docs/tutorial2.bdf", "wb") as handle:
    ...     writer.write_bdf(font, handle)

Once again,
the path ``docs/tutorial2.bdf``
can be whatever path you like.
