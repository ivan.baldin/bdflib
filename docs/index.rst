.. bdflib documentation master file, created by
   sphinx-quickstart on Sat Oct 27 13:18:54 2018.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to bdflib's documentation!
==================================

bdflib is a Python library for working with `BDF fonts`_.

The library allows for manipulating fonts directly and comes with
command-line utilities for performing various operations on font files.

.. _BDF fonts: http://en.wikipedia.org/wiki/BDF_%28Glyph_Bitmap_Distribution_Format%29

.. toctree::
   :maxdepth: 2

   about
   tutorial
   reference

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
